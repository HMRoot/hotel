package premises;

public abstract class Room {

    public boolean booked;
    public boolean occupied;

    public abstract void openDoor();

    public void book()
    {
        booked = true;
    }

    public void cancelBooking()
    {
        booked = false;
    }

    public void occupy()
    {
        occupied = true;
    }

    public void release()
    {
        occupied = false;
    }
}
