package premises;

import features.AirConditioned;

public class AirConditionedRoom extends Room implements AirConditioned {

    @Override
    public void openDoor() {

    }

    @Override
    public void changeTemperature() {

    }
}
