import premises.Room;

public class Hotel {

    public void book(Room room) {
        room.book();
    }

}
